#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <fcntl.h>


#include "DatosMemCompartida.h"



int main()
{
	
	int fd;
	DatosMemCompartida *mem;

	
	fd=open("FICH", O_RDWR);
	mem=static_cast<DatosMemCompartida*>(mmap(0, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0));
	close(fd);
	
	
	while(mem != NULL)
	{
		
		if(mem->esfera.centro.y < ((mem->raqueta.y1 + mem->raqueta.y2)/2))
			mem->accion=-1;
			
		else if(mem->esfera.centro.y > ((mem->raqueta.y1 + mem->raqueta.y2)/2))
			mem->accion=1;
		else
			mem->accion=0;
		
		usleep(25000);
			
	}
	
	munmap(mem, sizeof(DatosMemCompartida));
	return 0;







}
	
